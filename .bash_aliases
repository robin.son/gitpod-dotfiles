alias redis-start='docker run --rm -d --name redis -p 6379:6379 redis'
alias redis-stop='docker stop redis'
alias psql-local='psql postgresql://postgres:docker@localhost:5432'
alias postgres-start='docker run --rm --name postgres -v /workspace/remote-dev/postgres/data:/var/lib/postgresql/data -d -e POSTGRES_PASSWORD=localhost123 -p 5432:5432 postgres:13.2-alpine'
alias postgres-stop='docker stop postgres'


alias pf='function pf () {aws --profile staging-eks eks update-kubeconfig --name snaptravel-staging-eks; kubectl port-forward service/$1 8080:80;};pf'

git config --global alias.test '!PIPENV_DOTENV_LOCATION=.env.test pipenv run git-test'

function dev_gitlint_setup(){
	echo "Setting up git lint command"
	pip install pylint
	pip3 install git+https://github.com/snaptravel/git-lint.git
}

function setup_payments_files(){
	cp -a ~/.dotfiles/repo-files/payments/.* ./
}
function setup_payments(){
	echo "Setting up Payments repo"
	setup_payments_files
	super local 3.9.9
	docker-compose pull
}
function start_payments(){
	echo "Starting payments services. Use the VSCode debugger to run the service"
	docker-compose up -d postgres redis adminer
}

function setup_static_pages_node_env(){
	nvm install 10.23.0
	nvm use 10.23.0
}
function setup_static_pages_dotenv(){
	super vault get static-pages staging > .env
}
function setup_static_pages(){
	echo "Setting up static-pages repo"
	setup_static_pages_node_env
	setup_static_pages_dotenv
	npm install -g yarn
	yarn
}

function setup_front_end_node_env(){
	nvm install v18.15.0
	nvm use v18.15.0
}
function setup_front_end_dotenv(){
	super vault get front-end staging > .env
}
function setup_front_end(){
	echo "Setting up front_end repo"
	setup_front_end_node_env
	setup_front_end_dotenv
	npm install -g yarn
	yarn
}

export PIPENV_VENV_IN_PROJECT=1

export STAGING_POSTGRES_HOST="vpc-remote-hotel-staging.c0qfsszbrxht.us-east-1.rds.amazonaws.com"

alias rds-get-credentials-staging="aws --profile rds-admin rds generate-db-auth-token --hostname $STAGING_POSTGRES_HOST --port 5432 --region us-east-1 --username administrator"

alias aws-eks-staging='aws --profile staging-eks eks update-kubeconfig --name snaptravel-staging-eks'
alias pfk8='function pfk8 () {kubectl port-forward service/$1 $2:80;};pfk8'

export GITLAB_TEXTEMMA_URL='https://gitlab.com/textemma'
export GITLAB_TEXTEMMA_URL_SSH='git@gitlab.com:textemma'
alias clone-textemma-repo='function cloneter () { git clone $GITLAB_TEXTEMMA_URL/$1.git $2 $3;};cloneter'
alias clone-textemma-repo-ssh='function cloneterssh () { git clone $GITLAB_TEXTEMMA_URL_SSH/$1.git $2 $3;};cloneterssh'

alias poetry-venv-local='poetry config virtualenvs.in-project true'

alias clone-all='clone-textemma-repo recommender & clone-textemma-repo booking-service & clone-textemma-repo providers_hub & clone-textemma-repo rates-v2 & clone-textemma-repo front_end'