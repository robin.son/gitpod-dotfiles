import yaml

new_yaml_str = open("./gitpod.yml").read()
workspace_yaml_str = open("/workspace/remote-dev/.gitpod.yml").read()

new_yaml = yaml.safe_load(new_yaml_str)
workspace_yaml = yaml.safe_load(workspace_yaml_str)

new_extensions_list = new_yaml["vscode"]["extensions"]
workspace_extensions_list = workspace_yaml["vscode"]["extensions"]

combined_extensions_list = new_extensions_list + workspace_extensions_list
combined_extensions = set()

for ext in combined_extensions_list:
    combined_extensions.add(ext)

workspace_yaml["vscode"]["extensions"] = list(combined_extensions)

yaml_out = yaml.dump(workspace_yaml)
print(f"{yaml_out}")

workspace_yaml_file_out = open("/workspace/remote-dev/.gitpod.yml", "w")
print("Dumping Yaml...")
workspace_yaml_file_out.write(yaml_out)
