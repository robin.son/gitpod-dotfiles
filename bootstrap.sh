#!/bin/bash
. .bash_aliases

shopt -s dotglob

for f in ~/.dotfiles/*
do
	echo "Linking $f"
	ln -s $f ~/
done

echo "Linking SSH Config"
mkdir -p ~/.ssh
ln -s ~/.dotfiles/ssh/config ~/.ssh/config

echo "Linking scripts"
for f in ~/.dotfiles/remote-dev-scripts/*
do
	echo "Linking $f"
	ln -s $f /workspace/remote-dev
done

echo "Configuring VSCode Extensions..."
cd ~/.dotfiles/gitpod-yaml
poetry install
python ./configure-gitpod.py

shopt -u dotglob