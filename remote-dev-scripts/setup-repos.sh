#!/bin/bash
echo "Setup repos"

shopt -s dotglob

GITLAB_TEXTEMMA_URL='https://gitlab.com/textemma'
# Copy stashed envs
for dir in ~/.dotfiles/repos/*
do
	echo "Restoring files from $dir"
	PROJECT_DIRECTORY=$(basename $dir)
	PROJECT_PATH="/workspace/remote-dev/$PROJECT_DIRECTORY"
	echo "    Checking Project $PROJECT_DIRECTORY"
	if [ ! -d "$PROJECT_PATH" ]; then
		echo "    $PROJECT_DIRECTORY does not exist, attempting to clone..."
		git clone $GITLAB_TEXTEMMA_URL/$PROJECT_DIRECTORY.git $PROJECT_PATH
	fi
	for f in $dir/*
		do
			FILE_NAME=$(basename $f)
			RESTORE_PATH="$PROJECT_PATH/$FILE_NAME"
			echo "    $f -> $RESTORE_PATH"
			cp -r $f $RESTORE_PATH
		done
done

# Other Repos
FLASK_EXAMPLE_DIR="/workspace/remote-dev/braintree_flask_example"
git clone https://gitlab.com/ryan.lawson1/braintree_flask_example.git $FLASK_EXAMPLE_DIR
cp -r ~/.dotfiles/other-repos/braintree_flask_example/* $FLASK_EXAMPLE_DIR

echo "Exec repo scripts"

# Setup each repo with custom script
for dir in ~/.dotfiles/repo-scripts/*
do
	PROJECT_DIRECTORY=$(basename $dir)
	PROJECT_PATH="/workspace/remote-dev/$PROJECT_DIRECTORY"
	for f in $dir/*
		do
			FILE_NAME=$(basename $f)
			echo "    Exec $FILE_NAME -> $PROJECT_PATH"
			pushd $PROJECT_PATH
			$f
			popd
		done
done

shopt -u dotglob